# Adds `~/.local/bin` to $PATH
export PATH="$PATH:${$(find ~/.local/bin -type d -printf %p:)%%:}"
export PATH="$PATH:${$(find ~/go/bin -type d -printf %p:)%%:}"

source "$HOME/.cargo/env"
