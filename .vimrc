" File: .vimrc
"
" Author: Richard Schembri
" References: https://vimawesome.com

" Needs to be first
set nocompatible

filetype off
filetype plugin on

set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

Plugin 'VundleVim/Vundle.vim'

" ----- Vim Appearance ------------------------------------------------
Plugin 'vim-airline/vim-airline'
Plugin 'vim-airline/vim-airline-themes'
" - Theme -
Plugin 'tomasiser/vim-code-dark'
" Zoom window
Plugin 'troydm/zoomwintab.vim'

" ----- Search functionality ---- -------------------------------------
" Full path fuzzy file, buffer, mru, tag, ... finder for Vim
Plugin 'junegunn/fzf'
Plugin 'junegunn/fzf.vim'
" Search directories recursively with grep
Plugin 'jremmen/vim-ripgrep'

" ----- IDE like functionality ----------------------------------------
" File browser pane
Plugin 'scrooloose/nerdtree'
Plugin 'jistr/vim-nerdtree-tabs'
" syntax checker
"Plugin 'vim-syntastic/syntastic'
" Asynchronous syntax check
" Plugin 'dense-analysis/ale'
Plugin 'majutsushi/tagbar'

" Code auto completion tool
" Plugin 'valloric/youcompleteme'

" ----- Language/Framework specific --------------------------

" HTML
" Automaticall insert the closing HTML tag // https://vimawesome.com/plugin/html-autoclosetag-looking-forward
Plugin 'HTML-AutoCloseTag'

" Rust // :CocInstall coc-rust-analyzer
" Plugin 'rust-analyzer/rust-analyzer'

" ----- Syntax Highlighting --------------------------------

" Colored Parentheses
Plugin 'kien/rainbow_parentheses.vim'

" Rainbow color CSV data + sql type command support
Plugin 'mechatroner/rainbow_csv'

" automatic closing of quotes, parenthesis, brackets, etc.
Plugin 'Raimondi/delimitMate'

" ----- Git Support -----------------------------------------
" https://github.com/airblade/vim-gitgutter
" Plugin 'airblade/vim-gitgutter'
Plugin 'tpope/vim-fugitive'

" ---- Productivity ---------------------------------------------------
Plugin 'vimwiki/vimwiki'

" ---- Extras/Advanced plugins ----------------------------------------
" Highlight and strip trailing whitespace
" Plugin 'ntpeters/vim-better-whitespace'
" Easily surround chunks of text
Plugin 'tpope/vim-surround'
" Align CSV files at commas, align Markdown tables, and more
Plugin 'godlygeek/tabular'
" Syntax highlighting, matching rules and mappings for the original Markdown and extensions
Plugin 'plasticboy/vim-markdown'

" Multi cursor // https://github.com/terryma/vim-multiple-cursors
Plugin 'terryma/vim-multiple-cursors'

" Ledger support
Plugin 'ledger/vim-ledger'

" Bookmarks https://github.com/MattesGroeger/vim-bookmarks
Plugin 'MattesGroeger/vim-bookmarks'

" Visualize undo history in tree form
Plugin 'mbbill/undotree'


call vundle#end()

" --- General Settings ---
" line number, the column number, the virtual column number, and the relative position
set ruler
" display line numbers in the left margin
set number
set relativenumber
" Show vim commands
set showcmd
" Jump to search item whilst typing
set incsearch
" Highlight logical search match
set hlsearch

" Better case sensitive search:
" If you search for something containing uppercase characters,
" it will do a case sensitive search;
" If you search for something purely lowercase,
" it will do a case insensitive search.
set ignorecase
set smartcase

" For DevIcons
set encoding=utf8

set guifont=DroidSansMono\ Nerd\ Font\ 11

" set a map leader for more key combos
let mapleader = ' '
nnoremap <C-h> <C-w>h
nnoremap <C-j> <C-w>j
nnoremap <C-k> <C-w>k
nnoremap <C-l> <C-w>l

nnoremap <silent> <Leader>+ :vertical resize +5<CR>
nnoremap <silent> <Leader>- :vertical resize -5<CR>

" Turn on syntax highlighting
syntax on
" Turn on mouse support
" set mouse=a
map <F3> <ESC>:exec &mouse!=""? "set mouse=" : "set mouse=nv"<CR>

" Show a line for column limit
set colorcolumn=80

set tabstop=4 softtabstop=4
set shiftwidth=4
set smartindent

set undodir=~/.vim/undodir
nnoremap <leader>u :UndotreeToggle<CR>

" Copy to clipboard by default
if system('uname -s') == "Darwin\n"
  set clipboard=unnamed "OSX
else
  set clipboard=unnamedplus "Linux
endif

colorscheme codedark

" ----- vimwiki -----
let g:vimwiki_ext2syntax = {'.md': 'markdown', '.markdown': 'markdown', '.mdown': 'markdown'}
let g:vimwiki_global_ext = 0

let g:vimwiki_list = [{'path': '~/Documents/Notes/public-notes', 'path_html': '~/Documents/Notes/public-notes-html/', 'ext': '.md'},
            \ {'path': '~/Documents/Notes/private-notes', 'path_html': '~/Documents/Notes/private-notes-html/', 'ext': '.md'}]

" ----- Fuzzy find search -----
set nocompatible " Limit search to project
set path+=** "Search subdirectories recursively
set wildmenu "Show search results in one line

" ----- bling/vim-airline settings -----
" Always show statusbar
set laststatus=2

" Show airline for tabs too
let g:airline#extensions#tabline#enabled = 1
" ALE support
let g:airline#extensions#ale#enabled = 1

let g:airline_powerline_fonts = 1

let g:airline_theme = 'codedark'

" ----- dense-analysis/ale ---------
let g:ale_set_balloons = 1

" ----- scrooloose/nerdtree ---------
nnoremap <leader>pv :NERDTreeToggle<CR>
" ----- jistr/vim-nerdtree-tabs -----
" Open/close NERDTree Tabs with \t
nmap <silent> <leader>t :NERDTreeTabsToggle<CR>

" ----- troydm/zoomwintab -----------
nmap <silent> <leader>m :ZoomWinTabToggle<CR>
" enable/disable zoomwintab integration >
let g:airline#extensions#zoomwintab#enabled = 1

" jremmen/vim-ripgrep
" Allows ripgrep to automatically find the git root
if executable('rg')
	let g:rg_derive_root='true'
endif

" ripgrep: Project Search
nnoremap <Leader>ps :Rg<SPACE>

" valloric/youcompleteme
" Go to Definition
" nnoremap <silent> <Leader>gd :YcmCompleter GoTo<CR>

" -----  neoclide/coc.nvim (Conquer of Completion) -----------
" TextEdit might fail if hidden is not set.
set hidden

" Some servers have issues with backup files, see #649.
set nobackup
set nowritebackup

" Give more space for displaying messages.
set cmdheight=2

" Having longer updatetime (default is 4000 ms = 4 s) leads to noticeable
" delays and poor user experience.
set updatetime=300

" Don't pass messages to |ins-completion-menu|.
set shortmess+=c

command! -nargs=0 Prettier :CocCommand prettier.formatFile
" Use tab for trigger completion with characters ahead and navigate.
" inoremap <silent><expr> <TAB>
"       \ pumvisible() ? "\<C-n>" :
"       \ <SID>check_back_space() ? "\<TAB>" :
"       \ coc#refresh()
" inoremap <expr><S-TAB> pumvisible() ? "\<C-p>" : "\<C-h>"

function! s:check_back_space() abort
  let col = col('.') - 1
  return !col || getline('.')[col - 1]  =~# '\s'
endfunction

let g:coc_global_extensions=[ 'coc-omnisharp', 'coc-rust-analyzer' ]

" GoTo code navigation.
" GoTo code navigation.
nmap <silent> gd <Plug>(coc-definition)
nmap <silent> gy <Plug>(coc-type-definition)
nmap <silent> gi <Plug>(coc-implementation)
nmap <silent> gr <Plug>(coc-references)
nmap <leader>nm <Plug>(coc-rename)
nmap <leader>g[ <Plug>(coc-diagnostic-prev)
nmap <leader>g] <Plug>(coc-diagnostic-next)
nmap <silent> <leader>gp <Plug>(coc-diagnostic-prev-error)
nmap <silent> <leader>gn <Plug>(coc-diagnostic-next-error)
nnoremap <leader>cr :CocRestart

" -----  junegunn/fzf (Fuzzy finder) -----------
nnoremap <C-p> :Files<CR>

autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif

" Markdown settings
let g:markdown_fenced_languages = ['c++=cpp', 'viml=vim', 'bash=sh', 'ini=dosini', 'csharp=cs']
" - Markdown Preview settings
"Uncomment to override defaults:
"let g:instant_markdown_slow = 1
"let g:instant_markdown_autostart = 0
"let g:instant_markdown_open_to_the_world = 1
"let g:instant_markdown_allow_unsafe_content = 1
"let g:instant_markdown_allow_external_content = 0
"let g:instant_markdown_mathjax = 1
"let g:instant_markdown_logfile = '/tmp/instant_markdown.log'
"let g:instant_markdown_autoscroll = 0
"let g:instant_markdown_port = 8090
"let g:instant_markdown_python = 1

" Highlight trailing white spaces
highlight ExtraWhitespace ctermbg=red guibg=red
match ExtraWhitespace /\s\+$/
autocmd BufWinEnter * match ExtraWhitespace /\s\+$/
autocmd InsertEnter * match ExtraWhitespace /\s\+\%#\@<!$/
autocmd InsertLeave * match ExtraWhitespace /\s\+$/
autocmd BufWinLeave * call clearmatches()

" Visible tabs
"":set listchars=eol:⏎,tab:↹·,trail:~,nbsp:⎵,space:␣
:set listchars=eol:⏎,tab:↹·,nbsp:⎵,extends:»,precedes:«
:set list
set showbreak=↪\ 

" Rainbow Parentheses
au VimEnter * RainbowParenthesesToggle
au Syntax * RainbowParenthesesLoadRound
au Syntax * RainbowParenthesesLoadSquare
au Syntax * RainbowParenthesesLoadBraces

" ------------- FuGITive -------------
nmap <leader>gh :diffget //3<CR>
nmap <leader>gu :diffget //2<CR>
nmap <leader>gs :G<CR>

" -------- SirVer/ultisnips ---------

" Snippets are separated from the engine. Add this if you want them:
Plugin 'honza/vim-snippets'

" Trigger configuration. Do not use <tab> if you use https://github.com/Valloric/YouCompleteMe.
let g:UltiSnipsExpandTrigger="<leader><tab>"
let g:UltiSnipsJumpForwardTrigger="<c-b>"
let g:UltiSnipsJumpBackwardTrigger="<c-z>"

" If you want :UltiSnipsEdit to split your window.
let g:UltiSnipsEditSplit="vertical"

" ------------- CSharp --------------
"  Fold CSharp region by typing zf%
let b:match_words = '\s*#\s*region.*$:\s*#\s*endregion'
